## error-handler-library

An advanced mule 4 error handler library with handler overriding and extension. Not to be confused with error handler plugin. This is a comprehensive library to handle different types of errors (your could update the framework to send cloudhub notifications, create service now tickets, write to MQ, BD etc per error type)

Design document: https://signa-sportsunited.atlassian.net/wiki/spaces/MULE/pages/309362771/Mule+4+Error+Handling+Framework
Mule4 Error Handling Features: https://signa-sportsunited.atlassian.net/wiki/spaces/MULE/pages/303333437/Mule-4+Error+Handling+Features

---

## Getting Started

As per Organization needs, modify error handler library by

1. Add new error types
2. Modify default error message
3. Modify Error processing logic

**Steps to UPDATE**

1. Clone the repository
2. Update library as required
3. This can be published to Exchange by updating the **project.groupId** (Use Org Id of respective Anypoint Exchange). Best practice would be to use artefact repo.
(Make sure you have appropriate credentials configured in settings.xml file in local .m2 folder)
4. Run mvn deploy from project root folder to Publish this library to your/customer artifact repo (Nexus, artefactory, azure repo etc).



---

## How to Use

Create new API/ Update Existing API with Error handler library

**Steps to UPDATE**

1. Please make sure you have user credentials in settings.xml file in local .m2 folder
2. Publish **Json logger** in Customer Exchange
3. Add json-logger to project if it has not been added before and Configure json-logger global element
4. In Project pom.xml file add the below plugin in <build> session along with other plugins.
---
```
<build>
....
</plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <version>${maven.dependency.plugin.version}</version>
                <executions>
                    <execution>
                        <id>unpack</id>
                        <phase>generate-sources</phase>
                        <goals>
                            <goal>unpack</goal>
                        </goals>
                        <configuration>
                            <artifactItems>

                                <artifactItem>
                                    <!-- *************************************************** -->
                                    <!-- TODO: Replace with the Published error-handling artifact library details -->
                                    <!-- *************************************************** -->
                                    <groupId>b144f004-790a-4e9b-8ff3-61a28db48356</groupId>
                                    <artifactId>error-handler-library</artifactId>
                                    <version>${replace-with-latest-version}</version>
                                    <!-- *************************************************** -->
                                    <classifier>mule-application</classifier>
                                    <overWrite>true</overWrite>
                                    <outputDirectory>src/main/mule/</outputDirectory>
                                    <includes>**/error-*.xml</includes>
                                </artifactItem>
                                <artifactItem>
                                    <!-- *************************************************** -->
                                    <!-- TODO: Replace with the Published error-handling artifact library details -->
                                    <!-- *************************************************** -->
                                    <groupId>b144f004-790a-4e9b-8ff3-61a28db48356</groupId>
                                    <artifactId>error-handler-library</artifactId>
                                    <version>1.0.0-SNAPSHOT</version>
                                    <!-- *************************************************** -->
                                    <classifier>mule-application</classifier>
                                    <overWrite>true</overWrite>
                                    <outputDirectory>src/main/resources/</outputDirectory>
                                    <includes>**/*.yaml</includes>
                                </artifactItem>
                            </artifactItems>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
...
        </plugins>
    </build>
```
---
**Steps to UPDATE (contd.)**

5. Once the Artifact is embedded to your project, Go to Project root folder and execute mvn clean install . This should pull the library files from Exchange or Artefact repo.
6. Configure properties file (Copy the properties from error-properties-template.yaml to another properties file e.g., error-properties.yaml )
7. Add flow reference to **error-mainErrorHandlerFlow** in On Error Propagate or On Error Continue to initiate the error handler libary logic.
If this framework is used as a common-error handler, then add the error scope in the APIKit Main flow/ the main trigger flow of the app.
8. Make sure to configure http-listener response (Success and Failure) accordingly.


Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).